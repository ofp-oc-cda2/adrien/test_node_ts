# Sources
 - https://blog.appsignal.com/2022/01/19/how-to-set-up-a-nodejs-project-with-typescript.html

# use command

 - compile : **npx tsc**
 - start : **node src/main.js**
 - compile AND start: **npx ts-node src/main.ts** (after install **npm install ts-node --save-dev**) => cette methode est plus rapide mais bug lors de changement imports/exports.