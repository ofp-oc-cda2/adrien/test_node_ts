"use strict";
// [X] ajouter un arg pour type de périodes (mois/années)
//    [ ] traitement de l'arg
// [ ] ajouter un arg pour addition/moyenne
//    [ ] moyenne par jours? mois? semaines (haha) ?!?
Object.defineProperty(exports, "__esModule", { value: true });
exports.calendarator = void 0;
function calendarator(fetchCalForField, numberOfPeriod, periodsTypes = 'month') {
    if ((periodsTypes !== 'month') && (periodsTypes !== 'year')) {
        return 'error';
    }
    console.log(periodsTypes);
    // console.log('calendarator');
    const allPeriods = [];
    const dateRound = new Date();
    let thisPeriodYear = dateRound.getFullYear();
    let thisPeriodmonth = dateRound.getMonth() + 1;
    const labelsOfPeriods = [];
    if (periodsTypes === 'month') {
        for (let MonthIndex = 0; MonthIndex < numberOfPeriod; MonthIndex += 1) {
            const thisPeriod = `${thisPeriodYear}-${thisPeriodmonth}`;
            labelsOfPeriods.push(thisPeriod);
            const thisMonth = [];
            for (let index = 0; index < fetchCalForField.length; index++) {
                const element = fetchCalForField[index];
                const dateForm = new Date(element.date);
                const elementYear = dateForm.getFullYear();
                const elementMonth = dateForm.getMonth() + 1;
                const elementPeriod = `${elementYear}-${elementMonth}`;
                if (elementPeriod === thisPeriod) {
                    thisMonth.push(element);
                }
            }
            allPeriods.push(thisMonth);
            if (thisPeriodmonth > 1) {
                thisPeriodmonth = thisPeriodmonth - 1;
            }
            else {
                thisPeriodmonth = 12;
                thisPeriodYear = thisPeriodYear - 1;
            }
        }
    }
    else if (periodsTypes === 'year') {
        for (let yearIndex = 0; yearIndex < numberOfPeriod; yearIndex += 1) {
            const thisPeriod = `${thisPeriodYear}`;
            labelsOfPeriods.push(thisPeriod);
            const thisYear = [];
            for (let index = 0; index < fetchCalForField.length; index++) {
                const element = fetchCalForField[index];
                const dateForm = new Date(element.date);
                const elementYear = dateForm.getFullYear();
                const elementPeriod = `${elementYear}`;
                if (elementPeriod === thisPeriod) {
                    thisYear.push(element);
                }
            }
            allPeriods.push(thisYear);
            thisPeriodYear = thisPeriodYear - 1;
        }
    }
    console.log('verif allPeriods : ', allPeriods);
    const valuesOfPeriods = [];
    for (let index = 0; index < allPeriods.length; index++) {
        const element = allPeriods[index];
        const initialValue = 0;
        const total = element.reduce((a, b) => a + b.value, initialValue);
        valuesOfPeriods.push(total);
    }
    return { labels: labelsOfPeriods, values: valuesOfPeriods };
}
exports.calendarator = calendarator;
module.exports = {
    calendarator,
};
