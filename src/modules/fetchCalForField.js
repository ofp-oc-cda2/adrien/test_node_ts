"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.groupCalendar = exports.returnTest = exports.sayMyName = void 0;
const fetchCalForField = [
    {
        "_id": "62e272ace749a89d879c6f81",
        "date": "2022-07-28T00:00:00.000Z",
        "value": 0,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    },
    {
        "_id": "62e272b1e749a89d879c6f86",
        "date": "2022-07-27T00:00:00.000Z",
        "value": 1,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    },
    {
        "_id": "62e272b9e749a89d879c6f8b",
        "date": "2022-07-26T00:00:00.000Z",
        "value": 2,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    },
    {
        "_id": "62e272bfe749a89d879c6f90",
        "date": "2022-07-25T00:00:00.000Z",
        "value": 4,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    },
    {
        "_id": "62e272cbe749a89d879c6f95",
        "date": "2022-07-24T00:00:00.000Z",
        "value": 8,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    },
    {
        "_id": "63071f5e7a091eefc27ee816",
        "date": "2022-08-24T00:00:00.000Z",
        "value": 5,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    },
    {
        "_id": "630728b87a091eefc27ee8ca",
        "date": "2022-08-25T00:00:00.000Z",
        "value": 20,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    },
    {
        "_id": "630754157a091eefc27eea23",
        "date": "2022-08-19T00:00:00.000Z",
        "value": 0,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    },
    {
        "_id": "630754207a091eefc27eea28",
        "date": "2022-08-20T00:00:00.000Z",
        "value": 10,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    },
    {
        "_id": "630754277a091eefc27eea2d",
        "date": "2022-08-21T00:00:00.000Z",
        "value": 0,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    },
    {
        "_id": "630754377a091eefc27eea32",
        "date": "2022-08-22T00:00:00.000Z",
        "value": 10,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    },
    {
        "_id": "630dfb7f4208fa93d45aef97",
        "date": "2021-12-31T00:00:00.000Z",
        "value": 10,
        "userId": "6234924b35e8c3add810b079",
        "fieldId": "625808fccc0ef7c39e1c1374",
        "__v": 0
    }
];
function sayMyName(name) {
    if (name === "Heisenberg") {
        console.log("You're right 👍");
    }
    else {
        console.log("You're wrong 👎");
    }
}
exports.sayMyName = sayMyName;
function returnTest(number) {
    return number * 2;
}
exports.returnTest = returnTest;
function groupCalendar(calendarList) {
    console.log('groupCalendar', calendarList);
}
exports.groupCalendar = groupCalendar;
module.exports = {
    fetchCalForField,
    sayMyName,
    returnTest,
    groupCalendar,
};
